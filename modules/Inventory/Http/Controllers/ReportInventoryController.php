<?php

namespace Modules\Inventory\Http\Controllers;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use App\Models\Tenant\Establishment;
use App\Models\Tenant\Company;
use App\Models\Tenant\Item;
use Modules\Inventory\Models\ItemWarehouse;
use Modules\Inventory\Models\InventarioItem;
use Modules\Inventory\Exports\InventoryExport;
use Modules\Inventory\Models\Warehouse;



use Carbon\Carbon;

class ReportInventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {


        if($request->warehouse_id)
        {

        }else
        {
            $request->estable_id='tenancy_pos';

        }

        if($request->warehouse_id && $request->warehouse_id != 'all')
        {

            
            $reports=  DB::table($request->estable_id.'.items')

            ->select('items.id','items.description',DB::raw('sum('.$request->estable_id.'.inventario_items.quantity) as quantity'),'sale_unit_price','purchase_unit_price',DB::raw("(select sum(quantity) from ".$request->estable_id.".inventory_kardex  where ".$request->estable_id.".inventory_kardex.item_id=".$request->estable_id.".items.id ) as stock"))
            ->leftjoin(''.$request->estable_id.'.inventario_items',''.$request->estable_id.'.inventario_items.item_id','=','items.id')            
            ->join(''.$request->estable_id.'.inventarios',''.$request->estable_id.'.inventarios.id','=',''.$request->estable_id.'.inventario_items.inventario_id')            
            ->join(''.$request->estable_id.'.warehouses',''.$request->estable_id.'.warehouses.id','=',''.$request->estable_id.'.inventario_items.warehouse_id')

->where(''.$request->estable_id.'.inventario_items.warehouse_id', $request->warehouse_id)
->groupBy('items.id','items.description',''.$request->estable_id.'.inventarios.created_at')
->orderBy(''.$request->estable_id.'.inventarios.created_at')->paginate(config('tenant.items_per_page'));

            // $reports = ItemWarehouse::with(['item'])->where('warehouse_id', $request->warehouse_id)->whereHas('item',function($q){
            //     $q->where([['item_type_id', '01'], ['unit_type_id', '!=','ZZ']]);
            //     $q->whereNotIsSet();
            // })->latest()->paginate(config('tenant.items_per_page'));
        }
        else{

            // $reports = ItemWarehouse::with(['item'])->whereHas('item',function($q){
            //     $q->where([['item_type_id', '01'], ['unit_type_id', '!=','ZZ']]);
            //     $q->whereNotIsSet();
            // })
          
    
$reports=  DB::table($request->estable_id.'.items')

->select('items.id','items.description',DB::raw('sum('.$request->estable_id.'.inventario_items.quantity) as quantity'),'sale_unit_price','purchase_unit_price',DB::raw("(select sum(quantity) from ".$request->estable_id.".inventory_kardex  where ".$request->estable_id.".inventory_kardex.item_id=".$request->estable_id.".items.id ) as stock"))

->leftjoin(''.$request->estable_id.'.inventario_items',''.$request->estable_id.'.inventario_items.item_id','=','items.id')

->join(''.$request->estable_id.'.inventarios',''.$request->estable_id.'.inventarios.id','=',''.$request->estable_id.'.inventario_items.inventario_id')

->join(''.$request->estable_id.'.warehouses',''.$request->estable_id.'.warehouses.id','=',''.$request->estable_id.'.inventario_items.warehouse_id')

->groupBy('items.id','items.description',''.$request->estable_id.'.inventarios.created_at')
->orderBy(''.$request->estable_id.'.inventarios.created_at')->paginate(config('tenant.items_per_page'));
        }


        $warehouses = Warehouse::select('id', 'description')->get();

        return view('inventory::reports.inventory.index', compact('reports', 'warehouses'));
    }

    /**
     * Search
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request) {

        $reports = ItemWarehouse::with(['item'])->whereHas('item', function($q){
            $q->where([['item_type_id', '01'], ['unit_type_id', '!=','ZZ']]);
            $q->whereNotIsSet();
        })->latest()->get();

        return view('inventory::reports.inventory.index', compact('reports'));
    }

    /**
     * PDF
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function pdf(Request $request) {

        $company = Company::first();
        $establishment = Establishment::first();
        ini_set('max_execution_time', 0);

        if($request->warehouse_id && $request->warehouse_id != 'all')
        {
            $reports=  DB::table($request->estable_id.'.items')
            ->select('items.id','items.description',DB::raw('sum('.$request->estable_id.'.inventario_items.quantity) as quantity'),'sale_unit_price','purchase_unit_price',DB::raw("(select sum(quantity) from ".$request->estable_id.".inventory_kardex  where ".$request->estable_id.".inventory_kardex.item_id=".$request->estable_id.".items.id ) as stock"))
            ->leftjoin(''.$request->estable_id.'.inventario_items',''.$request->estable_id.'.inventario_items.item_id','=','items.id')            
            ->join(''.$request->estable_id.'.inventarios',''.$request->estable_id.'.inventarios.id','=',''.$request->estable_id.'.inventario_items.inventario_id')            
            ->join(''.$request->estable_id.'.warehouses',''.$request->estable_id.'.warehouses.id','=',''.$request->estable_id.'.inventario_items.warehouse_id')
           
->where(''.$request->estable_id.'.inventario_items.warehouse_id', $request->warehouse_id)
->groupBy('items.id','items.description',''.$request->estable_id.'.inventarios.created_at')
            ->latest(''.$request->estable_id.'.inventarios.created_at')->get();
            
            // $reports = ItemWarehouse::with(['item'])->where('warehouse_id', $request->warehouse_id)->whereHas('item', function($q){
            //     $q->where([['item_type_id', '01'], ['unit_type_id', '!=','ZZ']]);
            //     $q->whereNotIsSet();
            // })->latest()->get();
        }
        else{

            $reports=  DB::table($request->estable_id.'.items')

            ->select('items.id','items.description',DB::raw('sum('.$request->estable_id.'.inventario_items.quantity) as quantity'),'sale_unit_price','purchase_unit_price',DB::raw("(select sum(quantity) from ".$request->estable_id.".inventory_kardex  where ".$request->estable_id.".inventory_kardex.item_id=".$request->estable_id.".items.id ) as stock"))
            
            ->leftjoin(''.$request->estable_id.'.inventario_items',''.$request->estable_id.'.inventario_items.item_id','=','items.id')
            
            ->join(''.$request->estable_id.'.inventarios',''.$request->estable_id.'.inventarios.id','=',''.$request->estable_id.'.inventario_items.inventario_id')
            
            ->join(''.$request->estable_id.'.warehouses',''.$request->estable_id.'.warehouses.id','=',''.$request->estable_id.'.inventario_items.warehouse_id')
            ->groupBy('items.id','items.description',''.$request->estable_id.'.inventarios.created_at')
            ->latest(''.$request->estable_id.'.inventarios.created_at')->get();
            // $reports = ItemWarehouse::with(['item'])->whereHas('item', function($q){
            //     $q->where([['item_type_id', '01'], ['unit_type_id', '!=','ZZ']]);
            //     $q->whereNotIsSet();
            // })->latest()->get();
        }



        $pdf = PDF::loadView('inventory::reports.inventory.report_pdf', compact("reports", "company", "establishment"));
        $filename = 'Reporte_Inventario'.date('YmdHis');

        return $pdf->download($filename.'.pdf');
    }

    /**
     * Excel
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function excel(Request $request) {
        $company = Company::first();
        $establishment = Establishment::first();


        if($request->warehouse_id && $request->warehouse_id != 'all')
        {
            $records=  DB::table($request->estable_id.'.items')

            ->select('items.id','items.description',DB::raw('sum('.$request->estable_id.'.inventario_items.quantity) as quantity'),'sale_unit_price','purchase_unit_price',DB::raw("(select sum(quantity) from ".$request->estable_id.".inventory_kardex  where ".$request->estable_id.".inventory_kardex.item_id=".$request->estable_id.".items.id ) as stock"))
            
            ->leftjoin(''.$request->estable_id.'.inventario_items',''.$request->estable_id.'.inventario_items.item_id','=','items.id')
            
            ->join(''.$request->estable_id.'.inventarios',''.$request->estable_id.'.inventarios.id','=',''.$request->estable_id.'.inventario_items.inventario_id')
            
            ->join(''.$request->estable_id.'.warehouses',''.$request->estable_id.'.warehouses.id','=',''.$request->estable_id.'.inventario_items.warehouse_id')
            
            
->where(''.$request->estable_id.'.inventario_items.warehouse_id', $request->warehouse_id)
->groupBy('items.id','items.description',''.$request->estable_id.'.inventarios.created_at')
            ->latest(''.$request->estable_id.'.inventarios.created_at')->get();
            // $records = ItemWarehouse::with(['item'])->where('warehouse_id', $request->warehouse_id)->whereHas('item', function($q){
            //     $q->where([['item_type_id', '01'], ['unit_type_id', '!=','ZZ']]);
            //     $q->whereNotIsSet();
            // })->latest()->get();

        }
        else{
            // $records = ItemWarehouse::with(['item'])->whereHas('item', function($q){
            //     $q->where([['item_type_id', '01'], ['unit_type_id', '!=','ZZ']]);
            //     $q->whereNotIsSet();
            // })->latest()->get();

            
            $reports=  DB::table($request->estable_id.'.items')

->select('items.id','items.description',DB::raw('sum('.$request->estable_id.'.inventario_items.quantity) as quantity'),'sale_unit_price','purchase_unit_price',DB::raw("(select sum(quantity) from ".$request->estable_id.".inventory_kardex  where ".$request->estable_id.".inventory_kardex.item_id=".$request->estable_id.".items.id ) as stock"))

->leftjoin(''.$request->estable_id.'.inventario_items',''.$request->estable_id.'.inventario_items.item_id','=','items.id')

->join(''.$request->estable_id.'.inventarios',''.$request->estable_id.'.inventarios.id','=',''.$request->estable_id.'.inventario_items.inventario_id')

->join(''.$request->estable_id.'.warehouses',''.$request->estable_id.'.warehouses.id','=',''.$request->estable_id.'.inventario_items.warehouse_id')

            ->groupBy('items.id','items.description',''.$request->estable_id.'.inventarios.created_at')
            ->latest(''.$request->estable_id.'inventarios.created_at')->get();

        }


        return (new InventoryExport)
            ->records($records)
            ->company($company)
            ->establishment($establishment)
            ->download('ReporteInv'.Carbon::now().'.xlsx');
    }
    
    public function remove(Request $request)
    {
        $result = DB::connection('tenant')->transaction(function () use ($request) {
            // dd($request->all());
            $item_id = $request->input('item_id');
            $warehouse_id = $request->input('warehouse_id');
            $quantity = $request->input('quantity');
            $quantity_remove = $request->input('quantity_remove');
            $lots = ($request->has('lots')) ? $request->input('lots'):[];

            //Transaction
            $item_warehouse = ItemWarehouse::where('item_id', $item_id)
                                           ->where('warehouse_id', $warehouse_id)
                                           ->first();
            if(!$item_warehouse) {
                return [
                    'success' => false,
                    'message' => 'El producto no se encuentra en el almacén indicado'
                ];
            }

            if($quantity < $quantity_remove) {
                return  [
                    'success' => false,
                    'message' => 'La cantidad a retirar no puede ser mayor al que se tiene en el almacén.'
                ];
            }

            // $item_warehouse->stock = $quantity - $quantity_remove;
            // $item_warehouse->save();

            $inventory = new Inventory();
            $inventory->type = 3;
            $inventory->description = 'Retirar';
            $inventory->item_id = $item_id;
            $inventory->warehouse_id = $warehouse_id;
            $inventory->quantity = $quantity_remove;
            $inventory->save();

            foreach ($lots as $lot){

                if($lot['has_sale']){

                    $item_lot = ItemLot::findOrFail($lot['id']);
                    $item_lot->delete();

                }

            }

            return  [
                'success' => true,
                'message' => 'Producto trasladado con éxito'
            ];
        });

        return $result;
    }
}
